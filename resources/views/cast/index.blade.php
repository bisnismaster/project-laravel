@extends('/layout.master')

@section('judul')
Cast
@endsection

@section('subjudul')
<a href="/cast/create" class="btn btn-success">Tambah Cast</a>
@endsection

@section('content')
<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Bio</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse($casts as $key => $cast)

        <tr>
            <td>{{$key + 1}}</td>
            <td>{{$cast->nama}}</td>
            <td>{{$cast->umur}}</td>
            <td>{{$cast->bio}}</td>
            <td>
                <form action="/cast/{{$cast->id}}" method="POST">
                  <a href="/cast/{{$cast->id}}" class="btn btn-info btn-sm">Detail</a>
                  <a href="/cast/{{$cast->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                  @method('delete')
                  @csrf
                  <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                </form>
            </td>
          </tr>

        @empty
        <tr>
            <td>Data Masih Kosong</td>
        </tr>
        @endforelse
    </tbody>
  </table>
  @endsection