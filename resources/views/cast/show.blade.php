@extends('/layout.master')

@section('judul')
Detail Cast {{$cast->nama}}
@endsection

@section('subjudul')
<a href="/cast" class="btn btn-primary btn-sm">Back</a>
@endsection

@section('content')

<p>Name : {{$cast->nama}}</p>
<p>Umur : {{$cast->umur}}</p>
<p>Bio : {{$cast->bio}}</p>

 @endsection