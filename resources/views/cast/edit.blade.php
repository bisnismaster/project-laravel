@extends('/layout.master')

@section('judul')
Edit Cast {{$cast->nama}}
@endsection

@section('subjudul')
<a href="/cast" class="btn btn-success btn-sm">Back</a>
@endsection

@section('content')
    <form action="/cast/{{$cast->id}}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
          <label>Cast Name</label>
          <input type="text" name="nama" class="form-control" value="{{$cast->nama}}">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
          <label>Cast Age</label>
          <input type="text" name="umur" class="form-control" value="{{$cast->umur}}">
        </div>
        @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Cast Bio</label>
            <textarea name="bio" class="form-control" cols="30" rows="10">{{$cast->bio}}</textarea>
          </div>
          @error('bio')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror
        <button type="submit" class="btn btn-primary">Update</button>
      </form>
@endsection
