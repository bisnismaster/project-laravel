@extends('/layout.master')

@section('judul')
Buat Account Baru!
@endsection

@section('subjudul')
Sign Up Form
@endsection

@section('content')
    <form action="/welcome" method="POST">
        @csrf
        <label>First name:</label><br><br>
        <input type="text" name="firstname"><br><br>
        <label>Last name:</label><br><br>
        <input type="text" name="lastname"><br><br>
        <label>Gender:</label><br><br>
        <input type="radio" name="gender" value="male">Male<br>
        <input type="radio" name="gender" value="female">Female<br>
        <input type="radio" name="gender" value="other">Other<br><br>
        <label>Nationality:</label><br><br>
        <select name="nationality">
            <option value="indonesian">Indonesian</option>
            <option value="american">American</option>
            <option value="english">English</option>
        </select><br><br>
        <label>Language Spoken:</label><br><br>
        <input type="checkbox" name="ls">Bahasa Indonesia<br>
        <input type="checkbox" name="ls">English<br>
        <input type="checkbox" name="ls">Other<br><br>
        <label>Bio:</label><br>
        <textarea name="bio" cols="30" rows="10"></textarea><br><br>

        <input type="submit" value="Sign Up">
    </form>
@endsection
