<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
    public function signup() {
        return view('form');
    }

    public function submit(Request $request) {
        //dd($request->all());
        $firstname = $request['firstname'];
        $lastname = $request['lastname'];
        return view('welcome', compact('firstname','lastname'));
    }
}
